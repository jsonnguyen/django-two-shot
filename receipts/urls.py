from urllib.parse import urlparse
from django.urls import path

from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    AccountListView,
    ExpenseCategoryCreateView,
    AccountCreateView,
)

urlpatterns = [
    
    path("", ReceiptListView.as_view(), name="receipts_home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryListView.as_view(), name="expense_categories"),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path("categories/create/", ExpenseCategoryCreateView.as_view(), name="expense_create"),
    path("accounts/create/", AccountCreateView.as_view(), name="create_account")

    ]
